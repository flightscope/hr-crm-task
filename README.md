## SuiteCRM 7.8.31  LTS


### What's in this repository ###

This is the git repository for the Human Resources job interview tasks.

This repository has been created to allow future employees to send their solutions.

### Contributing to the project ###

Fork this repository, read the tasks in [TASKS.md](TASKS.md) and after finishing the tasks, create a pull request.
