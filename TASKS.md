# Interview Tasks:

## Task 1

1. On `Lead` creation send an email notification to a certain group of users.
2. Commit your changes in the code.
3. Create a pull request to our repository.